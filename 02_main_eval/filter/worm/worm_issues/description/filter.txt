enzyme
receptor
binding
receptor AND binding
receptor AND membrane
"receptor complex"
channel
channel AND activity
channel AND membrane
transporter
transporter AND activity
transporter AND membrane
excretory
excretion
excretory OR excretion
secretory
secretion
secretory OR secretion
peptidase
aspartic
aspartate
aspartic OR aspartate
cystein
cystathionine
cystein OR cystathionine
serine
serine AND activity
threonine peptidase
threonine
threonine AND activity
threonine AND (kinase OR activity)
metallopeptidase
metallo
G protein-coupled receptor
G-protein coupled receptor
protein AND coupled AND receptor
enzyme inhibitor
inhibitor activity
tissue AND penetration
penetration
feeding
host antigen presentation
antigen presentation
