# Cytoscape Import

### Edges
* Import Network From File: all.edges.cor95.tsv
* Interaction Definition
  * Source Interaction: Column1/Gene.ID.1
  * Target Interaction: Column2/Gene.ID.2
* Advanced -> Show Text File Import Options
  * Delimiter: Tab (deselect Space!)
  * Column Names: Transfer first line as column names
* Preview:
  * Select all columns to import edge attributes
  * Check proper Column Type (right click on each column)
    * i.e. Cor.* -> Floating Point (except Cor.Dir)

### Node Attributes
* Import Table From File: all.nodes.tsv
* Importing Type
  * Import Data as: Node Table Columns
* Other Defaults seem to be fine

### Preprocessing
* Edit -> Remove Duplicated Edges (ignore edge direction)
* Edit -> Remove Self-Loops

### Visualization
* Apply Preferred Layout
* Node Color -> Origin (species)
* Node Size -> Degree (# Edges)
* Edge Color -> Correlation direction (neg/pos Rho)

### Filter
* only interspecies edges
  * Control Panel -> Select -> Add Column Filter
  * Edge: Connection is Inter (Case sensitive)
  * Select -> Nodes -> Nodes connected by selected Edges
  * File -> New -> Network -> From selected Nodes, selected Edges
  * New Network from Selection
* only interspecies nodes (but including both inter- and intra-species edges)
  * Control Panel -> Select -> Add Column Filter
  * Node: Interconnected is true
  * New Network from Selection
