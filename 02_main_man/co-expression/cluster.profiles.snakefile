from snakemake.utils import R

CLUSTER_ENRICH = ["01", "02", "04", "05", "manWorm", "manPig"]
CLUSTER_REGULA = ["01", "02", "04", "05", "manWorm", "manPig", "GS_09942", "GS_11251", "GS_12056", "L3E_01572"]
GROUP = {"01" : "pig",
         "02" : "pig",
         "04" : "pig",
         "05" : "pig",
         "manWorm" : "worm",
         "manPig" : "pig",
         "GS_09942": "pig",
         "GS_11251": "pig",
         "GS_12056": "pig",
         "L3E_01572": "pig"}


rule all:
    input:
        [expand("profiles/cluster.{cluster}.go.profile.all.pdf", cluster = CLUSTER_ENRICH)],
        [expand("regulation/cluster.{cluster}.{group}.regulation.pdf", group = GROUP[cluster], cluster = cluster) for cluster in CLUSTER_REGULA]

# profiles via GO.db and ggplot2
rule vis_go_profile_script:
    input:
        annot = lambda wc: "../../02_main_eval/annotation/" + GROUP[wc.cluster] + "/annotation.foreground.annot",
        degs  = lambda wc: "../../02_main_eval/annotation/" + GROUP[wc.cluster] + "/" + GROUP[wc.cluster] + ".degs.maxfc.tsv",
        enr   = "enrichment/cluster.{cluster}.fisher_fdr0.05_all.slim.txt",
        clust = "all.cluster.{cluster}.txt"
    output:
        pdf_all  = "profiles/cluster.{cluster}.go.profile.all.pdf",
        png_all  = "profiles/cluster.{cluster}.go.profile.all.png"
    params:
        out_prefix = "profiles/cluster.{cluster}."
    script:
        "cluster.profiles.R"
 
# remove gene identifier from test to slim file
rule enrich_blast2GO_fisher_slim:
    input:
       tests = "enrichment/cluster.{cluster}.fisher_fdr0.05_all.txt",
    output:
       tests = "enrichment/cluster.{cluster}.fisher_fdr0.05_all.slim.txt",
    shell:
        "cut -f 1-10 {input.tests} > {output.tests}"

# visualize regulation of cluster, i.e. number of up/down regulation genes
rule vis_reg_profile:
    input:
        degs  = lambda wc: "../../02_main_eval/annotation/" + GROUP[wc.cluster] + "/" + GROUP[wc.cluster] + ".degs.maxfc.tsv",
        clust = "all.cluster.{cluster}.txt"
    output:
        pdf_all  = "regulation/cluster.{cluster}.{group}.regulation.pdf",
        png_all  = "regulation/cluster.{cluster}.{group}.regulation.png"
    params:
        out_prefix = "regulation/cluster.{cluster}.{group}."
    script:
        "cluster.regulation.R"
