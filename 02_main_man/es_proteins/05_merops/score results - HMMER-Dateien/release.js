(function () {
'use strict';

var root = "/Tools/hmmer/";
var ms = 1000;

var disableSubmissionForm = function disableSubmissionForm() {
  var form = document.querySelector('#proteinSeqSearchForm, #hmmScanForm, #hmmSearchForm, #iterativeSearchForm');
  if (!form) return;
  Array.prototype.slice.call(form.querySelectorAll('input, textarea')).forEach(function (element) {
    return element.setAttribute('disabled', '');
  });
};

var main = function main() {
  var elements = Array.prototype.slice.call(document.querySelectorAll('.release-info'));

  return fetch(root + 'status/data', { headers: new Headers({ 'Content-Type': 'application/json' }) }).then(function (r) {
    return r.json();
  }).then(function (_ref) {
    var updating = _ref.updating,
        next_planned_release = _ref.next_planned_release;

    var now = new Date();
    // Epoch in s, “new Date()” takes ms, hence the conversions
    var release = new Date(next_planned_release * ms);
    if (isNaN(release.getTime())) throw new Error('Not a valid date');
    var deltaInDays = Math.floor(Math.max(0, release - now) / ms / 60 / 60 / 24);
    var fuzzyDateString = void 0;
    var important = false;
    if (updating) {
      disableSubmissionForm();
      fuzzyDateString = 'New release is currently ongoing';
      important = true;
    } else if (deltaInDays >= 30) {
      fuzzyDateString = 'Next release in more than a month';
    } else if (deltaInDays >= 7) {
      fuzzyDateString = 'Next release within a month';
    } else if (deltaInDays >= 1) {
      fuzzyDateString = 'Next release within a week, think about downloading your results';
      important = true;
    } else {
      fuzzyDateString = 'Next release is imminent, ' + 'avoid big jobs and download your results now';
      important = true;
    }
    elements.forEach(function (element) {
      if (element.classList.contains('important') && !important) {
        element.style.opacity = '0';
        return;
      }
      element.title = 'estimated local date: ' + release.toLocaleDateString();
      element.textContent = fuzzyDateString;
    });

    document.body.classList.add('with-release-info');
  });
};

var tryAndRunMain = function tryAndRunMain() {
  try {
    main().catch(function (error) {
      return void 0;
    });
  } catch (error) {
  }
};

if (document.readyState === 'loading') {
  document.addEventListener('DOMContentLoaded', function handleReady() {
    document.removeEventListener('DOMContentLoaded', handleReady);
    tryAndRunMain();
  });
} else {
  tryAndRunMain();
}

}());
//# sourceMappingURL=release.min.js.map
