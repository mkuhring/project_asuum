(function () {
'use strict';

var ebiSearch = "//www.ebi.ac.uk/ebisearch/ws/rest/";

var domainToSuperset = {
  genomes: 'Genes, genomes & variation',
  nucleotideSequences: 'Genes, genomes & variation',
  proteinSequences: 'Protein sequences, families & motifs',
  macromolecularStructures: 'Molecular structures',
  smallMolecules: 'Chemical biology',
  geneExpression: 'Gene, protein & metabolite expression',
  molecularInteractions: 'Systems',
  reactionsPathways: 'Systems',
  proteinFamilies: 'Protein sequences, families & motifs',
  proteinExpressionData: 'Gene, protein & metabolite expression',
  enzymes: 'Systems',
  literature: 'Literature & ontologies',
  ontologies: 'Literature & ontologies'
};

var availableSupersets = ['Genes, genomes & variation', 'Protein sequences, families & motifs', 'Molecular structures', 'Chemical biology', 'Gene, protein & metabolite expression', 'Systems', 'Literature & ontologies'];

var tooMuchRE = /\((\d+) found\)/i;

// globals jQuery: false
var _ref = function () {
  if ('Promise' in window && 'fetch' in window) {
    return {
      getJson: function getJson(url) {
        return fetch(url).then(function (r) {
          return r.json();
        });
      },
      promiseAll: function promiseAll(promiseLikeObjects) {
        return Promise.all(promiseLikeObjects);
      },

      CustomPromise: Promise
    };
  }
  return {
    getJson: function getJson(url) {
      return jQuery.getJSON(url);
    },
    promiseAll: function promiseAll(promiseLikeObjects) {
      var _jQuery;

      return (_jQuery = jQuery).when.apply(_jQuery, promiseLikeObjects).then(function () {
        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        return args;
      });
    },
    CustomPromise: function CustomPromise(cb) {
      var promise = jQuery.Deferred();
      cb(promise.resolve, promise.reject);
      return promise.promise();
    }
  };
}();
var getJson = _ref.getJson;
var promiseAll = _ref.promiseAll;
var CustomPromise = _ref.CustomPromise;

var getSubDomain2Domain = function getSubDomain2Domain() {
  return getJson(ebiSearch + '?format=json').then(function (data) {
    var subDomain2Domain = {};
    var subDomain2Name = {};
    // Recursive function to traverse the EBI resources
    var recFun = function recFun(domain, subDomains) {
      if (!subDomains) return;
      for (var _iterator = subDomains, _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
        var _ref3;

        if (_isArray) {
          if (_i >= _iterator.length) break;
          _ref3 = _iterator[_i++];
        } else {
          _i = _iterator.next();
          if (_i.done) break;
          _ref3 = _i.value;
        }

        var _ref2 = _ref3;
        var id = _ref2.id;
        var name = _ref2.name;
        var subdomains = _ref2.subdomains;

        subDomain2Domain[id] = domain;
        subDomain2Name[id] = name;
        recFun(domain, subdomains);
      }
    };
    // Root level, all the main subdomains of the EBI resources
    for (var _iterator2 = data.domains[0].subdomains, _isArray2 = Array.isArray(_iterator2), _i2 = 0, _iterator2 = _isArray2 ? _iterator2 : _iterator2[Symbol.iterator]();;) {
      var _ref5;

      if (_isArray2) {
        if (_i2 >= _iterator2.length) break;
        _ref5 = _iterator2[_i2++];
      } else {
        _i2 = _iterator2.next();
        if (_i2.done) break;
        _ref5 = _i2.value;
      }

      var _ref4 = _ref5;
      var id = _ref4.id;
      var subdomains = _ref4.subdomains;

      var superset = domainToSuperset[id];
      if (!superset) continue;
      // Call the recursive function
      recFun(superset, subdomains);
    }
    return { subDomain2Domain: subDomain2Domain, subDomain2Name: subDomain2Name };
  });
};

var getSubDomainsFor = function getSubDomainsFor(db, name) {
  return getJson('' + ebiSearch + db + '/entry/' + name + '/xref?format=json').then(function (data) {
    return data.domains.map(function (d) {
      return d.id;
    });
  });
};

var getSingleSubDomainLinks = function getSingleSubDomainLinks(db, name) {
  return function (subDomain) {
    return getJson(('\n  ' + ebiSearch + db + '/entry/' + name + '/xref/' + subDomain + '?format=json&fieldurl=true\n').trim()).then(function (data) {
      if (data && data.entries && data.entries[0] && data.entries[0].references) {
        return [subDomain, data.entries[0].references.map(function (_ref6) {
          var id = _ref6.id,
              source = _ref6.source,
              fieldURLs = _ref6.fieldURLs;
          return {
            id: id,
            source: source,
            url: (fieldURLs.find(function (o) {
              return o.name === 'main';
            }) || fieldURLs[0]).value
          };
        })];
      }
      if (data && data.diagnostics && data.diagnostics.message) {
        var message = data.diagnostics.message;

        var _ref7 = message.match(tooMuchRE) || [],
            count = _ref7[1];

        if (!count) {
          return [];
        }
        var query = 'db=' + db + '&id=' + name + '&ref=' + subDomain;
        return [subDomain, [{
          id: count + ' results (find on the EBI Search website)',
          url: 'https://www.ebi.ac.uk/ebisearch/crossrefsearch.ebi?' + query
        }]];
      }
      return [];
    });
  };
};

// Get data for the pop-up and construct the corresponding HTML string
var getPopUpContent = function getPopUpContent(subDomain2Name, db, name, subDomains) {
  return promiseAll(subDomains.map(getSingleSubDomainLinks(db, name))).then(function (data) {
    return ('\n  <ul>\n    ' + data.filter(function (_ref8) {
      var xrefs = _ref8[1];
      return xrefs && xrefs.length;
    }).map(function (_ref9) {
      var subDomain = _ref9[0],
          xrefs = _ref9[1];
      return ('\n        <li>' + (subDomain2Name[subDomain] || subDomain) + '\n          <ul>\n            ' + xrefs.map(function (_ref10) {
        var id = _ref10.id,
            url = _ref10.url;
        return ('\n              <li>\n                <a target="_blank" href="' + url + '">' + id + '</a>\n              </li>\n            ').trim();
      }).join('') + '\n          </ul>\n        </li>\n      ').trim();
    }).join('') + '\n  </ul>\n').trim();
  });
};

// recursive function, processing all in toBeProcessed
var process = function process(res, toBeProcessed, db, subDomain2Domain, subDomain2Name) {
  // escape case, resolve the promise and finish the recursion
  // when everything has been processed, no need to recurse anymore
  if (!toBeProcessed.length) {
    res();
    return;
  }
  // actual processing
  var cell = toBeProcessed.shift();
  var name = cell.dataset.name;

  if (!name) {
    // No name was found, problem here, skip the row
    cell.classList.add('error'); // Color all the available supersets
    // goes deeper in the recursion
    process(res, toBeProcessed, db, subDomain2Domain, subDomain2Name);
    return;
  }
  var p = getSubDomainsFor(db, name).then(function (subDomains) {
    var superset2subDomains = {};
    for (var _iterator3 = subDomains, _isArray3 = Array.isArray(_iterator3), _i3 = 0, _iterator3 = _isArray3 ? _iterator3 : _iterator3[Symbol.iterator]();;) {
      var _ref11;

      if (_isArray3) {
        if (_i3 >= _iterator3.length) break;
        _ref11 = _iterator3[_i3++];
      } else {
        _i3 = _iterator3.next();
        if (_i3.done) break;
        _ref11 = _i3.value;
      }

      var sd = _ref11;

      var superset = subDomain2Domain[sd];
      var sdSet = superset2subDomains[superset] || [];
      if (!sdSet.includes(sd)) sdSet.push(sd);
      superset2subDomains[superset] = sdSet;
    }
    var populatedSupersets = Object.keys(superset2subDomains);

    var _loop = function _loop(availableSuperset) {
      // get a “bubble” (UI element) corresponding to one of the supersets
      var bubble = cell.querySelector('[data-xref="' + availableSuperset + '"]');
      // this superset doesn't contain any cross-reference
      if (!populatedSupersets.includes(availableSuperset)) {
        bubble.classList.add('unavailable'); // Hide it
        return 'continue'; // Skip to next bubble
      }
      // this superset does contain cross-references
      bubble.tabIndex = '0'; // Add to normal tab navigation
      // Connect to qtip to load more data (links) on mouseover, focus, or click
      jQuery(bubble).qtip({
        content: function content(_, api) {
          getPopUpContent(subDomain2Name, db, name, superset2subDomains[availableSuperset]).then(function (htmlString) {
            return api.set('content.text', htmlString);
          });
          return 'Loading…';
        },

        show: { solo: true, event: 'mouseover focus click' },
        hide: { fixed: true, delay: 1000, event: 'mouseout blur' },
        position: { my: 'top left', at: 'center' },
        style: { classes: 'xref' }
      });
    };

    for (var _iterator4 = availableSupersets, _isArray4 = Array.isArray(_iterator4), _i4 = 0, _iterator4 = _isArray4 ? _iterator4 : _iterator4[Symbol.iterator]();;) {
      var _ref12;

      if (_isArray4) {
        if (_i4 >= _iterator4.length) break;
        _ref12 = _iterator4[_i4++];
      } else {
        _i4 = _iterator4.next();
        if (_i4.done) break;
        _ref12 = _i4.value;
      }

      var availableSuperset = _ref12;

      var _ret = _loop(availableSuperset);

      if (_ret === 'continue') continue;
    }
    cell.classList.add('resolved'); // Color all the available supersets
    // goes deeper in the recursion
    process(res, toBeProcessed, db, subDomain2Domain, subDomain2Name);
  });
  p[p.fail ? 'fail' : 'catch'](function (err) {
    // Some error happened, skip the row
    cell.classList.add('error');
    // goes deeper in the recursion
    process(res, toBeProcessed, db, subDomain2Domain, subDomain2Name);
  });
};

// function exposed globally, to be called after the results have been loaded
__HmmerGlobal.processXRefs = function () {
  try {
    return promiseAll(Array.prototype.slice.call(document.querySelectorAll('[data-db].unresolved-xrefs')).map(function (tableWithDB) {
      tableWithDB.classList.remove('unresolved-xrefs');
      var toBeProcessed = Array.prototype.slice.call(tableWithDB.querySelectorAll('[data-name]'));
      if (!toBeProcessed.length) return;
      // Tooltip on column
      var helpSrc = jQuery('.help-src');
      helpSrc.qtip({
        content: helpSrc.next('.xref-help'),
        show: { solo: true, event: 'mouseover focus click' },
        hide: { fixed: true, delay: 1000, event: 'mouseout blur' },
        position: { my: 'top left', at: 'center' }
      });
      //
      var db = tableWithDB.dataset.db;
      // console.log('processing x-refs');
      // main async recursive logic

      return getSubDomain2Domain().then(function (_ref13) {
        var subDomain2Domain = _ref13.subDomain2Domain,
            subDomain2Name = _ref13.subDomain2Name;
        return new CustomPromise(function (res) {
          process(res, toBeProcessed, db, subDomain2Domain, subDomain2Name);
        });
      });
    }));
  } catch (err) {
  }
};

// Call it once, if we already have the results on the page when script loaded
__HmmerGlobal.processXRefs();

}());
//# sourceMappingURL=results.min.js.map
