# analyze DEG proteins online
PROTEINS = "worm.deg.proteins.fasta" # from ../02_main_eval/proteins/worm/
IDMAPPING = "worm.all.trans.gene.ids" # from ../02_main_eval/proteins/worm/

SIGNALP = "01_signalp_4.1/Eukaryotes/gff.txt"
SECRETOMEP_BACN = ["02_secretomep_2.0_p1/Gram-negative bacteria/secretomep.txt",
                  "02_secretomep_2.0_p2/Gram-negative bacteria/secretomep.txt"]
SECRETOMEP_BACP = ["02_secretomep_2.0_p1/Gram-positive bacteria/secretomep.txt",
                  "02_secretomep_2.0_p2/Gram-positive bacteria/secretomep.txt"]
SECRETOMEP_MAM = ["02_secretomep_2.0_p1/Mammalian/secretomep.txt",
                  "02_secretomep_2.0_p2/Mammalian/secretomep.txt"]
TARGETP = "03_targetp_1.1/Non-plant/targetp.txt"
TMHMM = "04_tmhmm_2.0/tmhmm.txt"

MEROPS = "05_merops/merops.txt"

'''
Annotate ''' + PROTEINS + ''' with...
        
SignalP [http://www.cbs.dtu.dk/services/SignalP/]
1) Organism group: Eukaryotes
2) other parameters:
* D-cutoff values: Default (optimized for correlation)
* Output format: Short (no graphics)
* Method: Input sequences may include TM regions
3) save at least the gff (buttom of result page) to (resp.):
''' + SIGNALP + '''
        
SECRETOMEP [http://www.cbs.dtu.dk/services/SecretomeP/]
1a) Use fasta splits for bacteria, due to protein number limitations
1b) Use fasta shortid splits for Mammalian, due to ID/name length limitations
2) Organism group: Gram-negative bacteria, Gram-positive bacteria, Mammalian
3) save the htmls and copy the tables (without header) to (resp.):
''' + "\n".join(SECRETOMEP_BACN) + '''
''' + "\n".join(SECRETOMEP_BACP) + '''
''' + "\n".join(SECRETOMEP_MAM) + '''
        
TargetP [http://www.cbs.dtu.dk/services/TargetP/]
1) Organism group: Non-plant
2) Cutoffs: no cutoffs; winner-takes-all (default)
3) save the html and copy the table (without header) to :
''' + TARGETP + '''
    
TMHMM [http://www.cbs.dtu.dk/services/TMHMM/]
1) Output format: One line per protein
2) save the html and copy the table to :
''' + TMHMM + '''
'''

rule all:
    input:
        "plot.venn.sigp.secp.pdf",
        "plot.pie.es.prots.pdf",
        "plot.heatmap.leftovers.pdf",
        "plot.pie.es.ec.pdf",
        "plot.heatmap.merops.all.pdf",
        "plot.heatmap.merops.es.pdf"


### Eval and plot ES proteins ###

# merge SignalP and SecretomeP, substract TargetP (mitoch.) and TMHMM (TMs)
rule select_es_proteins:
    input:
        signalp = "01.signalp.deg.hits.txt",
        secretomep_bacn = "02.secretomep.bacn.deg.hits.txt",
        secretomep_bacp = "02.secretomep.bacp.deg.hits.txt",
        secretomep_mam = "02.secretomep.mam.deg.hits.txt",
        targetp = "03.targetp.mito.deg.hits.txt",
        tmhmm = "04.tmhmm.transm.deg.hits.txt"
    output:
        venn_secp_orgs = "plot.venn.secp.vars.pdf",
        venn_sigp_secp = "plot.venn.sigp.secp.pdf",
        es_pie = "plot.pie.es.prots.pdf",
        es_ids = "es.proteins.txt"
    shell:
        "Rscript es.proteins.R"

# merge ES proteins with annotations and fold changes
rule merge_es_proteins:
    input:
        degs = "../../02_main_eval/annotation/worm/degs.annotated.tsv",
        sigs = "es.proteins.txt"
    output:
        all = "worm.degs.es.proteins.txt"
    shell:
        "Rscript merge.degs.signal.R"

# plot ES proteins FC heatmap
rule plot_es_proteins_fc:
    input:
        degs = "worm.degs.es.proteins.txt"
    output:
        pdf = "plot.heatmap.leftovers.pdf",
        png = "plot.heatmap.leftovers.png"
    shell:
        "Rscript heatmap.degs.signal.R"

# plot ES protein EC classes, mapped via KEGG KO annotation
rule plot_es_proteins_ec:
    input:
        kegg = "../../02_main_eval/kegg/worm/degs.annotated_kegg.tsv",
        es = "es.proteins.txt"
    output:
        pie = "plot.pie.es.ec.pdf",
        data = "worm.degs.es.ec.txt"
    shell:
        "Rscript enzyme.classes.R"


# merge MEROPS proteins with annotations and fold changes
rule merge_merops_proteins:
    input:
        merops = "05.merops.degs.txt",
        es = "es.proteins.txt"
    output:
        all = "worm.degs.merops.all.txt",
        es = "worm.degs.merops.es.txt"
    shell:
        "Rscript merge.degs.merops.R"

# plot MEROPS all proteins FC heatmap
rule plot_merops_all_fc:
    input:
        degs = "worm.degs.merops.all.txt"
    output:
        pdf = "plot.heatmap.merops.all.pdf",
        png = "plot.heatmap.merops.all.png"
    shell:
        "Rscript heatmap.degs.merops.all.R"

# plot MEROPS es proteins FC heatmap
rule plot_merops_es_fc:
    input:
        degs = "worm.degs.merops.es.txt"
    output:
        pdf = "plot.heatmap.merops.es.pdf",
        png = "plot.heatmap.merops.es.png"
    shell:
        "Rscript heatmap.degs.merops.es.R"


### Extract/parse online database results ###

rule extract_01_signalp_deg_hits:
    input:
        signalp = SIGNALP,
        ids = IDMAPPING
    output:
        degs = "01.signalp.deg.hits.txt"
    run:
        # trans to gene mapping
        trans2gene = dict()
        with open(input.ids, 'r') as f_in:
            for line in f_in:
                splits = line.strip("\n").split("\t")
                gene_id = splits[1].replace(':', '_')
                trans_id = splits[0].replace(':', '_')
                trans2gene[trans_id] = gene_id
                
        # collect SignalP gene hits 
        deg_hits = set()
        with open(input.signalp, 'r') as f_in:
            for line in f_in:
                if not line.startswith("#"):
                    trans_id = re.sub(r"_ORF\d+$", "", line.strip("\n").split("\t")[0])
                    gene_id = trans2gene[trans_id]
                    deg_hits.add(gene_id)
                    
        with open(output.degs, 'w') as f_out:
            for deg in deg_hits:
                f_out.write(deg + "\n")

rule extract_02_secretomep_deg_hits:
    input:
        secretomep_bacn = SECRETOMEP_BACN,
        secretomep_bacp = SECRETOMEP_BACP,
        secretomep_mam = SECRETOMEP_MAM,
        ids = IDMAPPING
    output:
        degs_bacn = "02.secretomep.bacn.deg.hits.txt",
        degs_bacp = "02.secretomep.bacp.deg.hits.txt",
        degs_mam = "02.secretomep.mam.deg.hits.txt"
    run:
        # trans to gene mapping
        trans2gene = dict()
        with open(input.ids, 'r') as f_in:
            for line in f_in:
                splits = line.strip("\n").split("\t")
                gene_id = splits[1].replace(':', '_')
                trans_id = splits[0].replace(':', '_')
                trans2gene[trans_id] = gene_id

        # collect SecretomeP BAC- gene hits
        deg_hits = set()
        for file in input.secretomep_bacn:
            with open(file, 'r') as f_in:
                for line in f_in:
                    splits = line.strip("\n").split("\t")
                    if float(splits[3]) > 0.5: # if score > 0.5
                        trans_id = re.sub(r"_ORF\d+$", "", splits[4])
                        gene_id = trans2gene[trans_id]
                        deg_hits.add(gene_id)

        with open(output.degs_bacn, 'w') as f_out:
            for deg in deg_hits:
                f_out.write(deg + "\n")

        # collect SecretomeP BAC+ gene hits
        deg_hits = set()
        for file in input.secretomep_bacp:
            with open(file, 'r') as f_in:
                for line in f_in:
                    splits = line.strip("\n").split("\t")
                    if float(splits[3]) > 0.5: # if score > 0.5
                        trans_id = re.sub(r"_ORF\d+$", "", splits[4])
                        gene_id = trans2gene[trans_id]
                        deg_hits.add(gene_id)

        with open(output.degs_bacp, 'w') as f_out:
            for deg in deg_hits:
                f_out.write(deg + "\n")

        # collect SecretomeP Mammal gene hits
        deg_hits = set()
        for file in input.secretomep_mam:
            with open(file, 'r') as f_in:
                for line in f_in:
                    splits = line.strip("\n").split()
                    # if score > 0.6 (Mam is different than Bac) and not predicted by SignalP
                    if float(splits[1]) > 0.6 and splits[4] != "signal":
                        if "ORF" in splits[0]:
                            trans_id = "TCONS_000" + splits[0]
                        else:
                            trans_id = "transcript_" + splits[0]
                        trans_id = re.sub(r"_ORF\d+$", "", trans_id)
                        gene_id = trans2gene[trans_id]
                        deg_hits.add(gene_id)

        with open(output.degs_mam, 'w') as f_out:
            for deg in deg_hits:
                f_out.write(deg + "\n")

rule extract_03_targetp_mito_deg_hits:
    input:
        targetp = TARGETP,
        ids = IDMAPPING
    output:
        degs = "03.targetp.mito.deg.hits.txt"
    run:
        # trans to gene mapping
        trans2gene = dict()
        with open(input.ids, 'r') as f_in:
            for line in f_in:
                splits = line.strip("\n").split("\t")
                gene_id = splits[1].replace(':', '_')
                trans_id = splits[0].replace(':', '_')
                trans2gene[trans_id] = gene_id

        # collect TargetP mitochondrial gene hits
        deg_hits = set()
        with open(input.targetp, 'r') as f_in:
            for line in f_in:
                if not line.startswith("#"):
                    splits = re.split(r" +", line.strip("\n"))
                    if splits[5] == "M": # if mito prediction
                        trans_id = re.sub(r"_ORF\d+$", "", splits[0])
                        gene_id = trans2gene[trans_id]
                        deg_hits.add(gene_id)

        with open(output.degs, 'w') as f_out:
            for deg in deg_hits:
                f_out.write(deg + "\n")

rule extract_04_tmhmm_transm_deg_hits:
    input:
        tmhmm = TMHMM,
        ids = IDMAPPING
    output:
        degs = "04.tmhmm.transm.deg.hits.txt"
    run:
        # trans to gene mapping
        trans2gene = dict()
        with open(input.ids, 'r') as f_in:
            for line in f_in:
                splits = line.strip("\n").split("\t")
                gene_id = splits[1].replace(':', '_')
                trans_id = splits[0].replace(':', '_')
                trans2gene[trans_id] = gene_id

        # collect TargetP mitochondrial gene hits
        deg_hits = set()
        with open(input.tmhmm, 'r') as f_in:
            for line in f_in:
                if not line.startswith("#"):
                    splits = line.strip("\n").split("\t")
                    if splits[4] != "PredHel=0": # if transmembrane (i.e. helices) predicted
                        trans_id = re.sub(r"_ORF\d+$", "", splits[0])
                        gene_id = trans2gene[trans_id]
                        deg_hits.add(gene_id)

        with open(output.degs, 'w') as f_out:
            for deg in deg_hits:
                f_out.write(deg + "\n")

rule extract_05_merops_degs:
    input:
        merops = MEROPS,
        ids = IDMAPPING
    output:
        degs = "05.merops.degs.txt"
    run:
        # trans to gene mapping
        trans2gene = dict()
        with open(input.ids, 'r') as f_in:
            for line in f_in:
                splits = line.strip("\n").split("\t")
                gene_id = splits[1].replace(':', '_')
                trans_id = splits[0].replace(':', '_')
                trans2gene[trans_id] = gene_id

        # collect TargetP mitochondrial gene hits
        deg_hits = set()
        with open(input.merops, 'r') as f_in:
            with open(output.degs, 'w') as f_out:
                header_splits = [split.strip() for split in f_in.readline().strip("\n").split("\t")]
                header_splits[0] = "Gene.ID"
                header_splits[1] = "Transcript.ID"
                f_out.write("\t".join(header_splits) + "\n")
                for line in f_in:
                    splits = [split.strip() for split in line.strip("\n").split("\t")]
                    print(splits)
                    if int(splits[2]) > 0: # has MEROPS hits
                        trans_id = re.sub(r"_ORF\d+$", "", splits[1])
                        gene_id = trans2gene[trans_id]
                        splits[0] = gene_id
                        f_out.write("\t".join(splits) + "\n")
