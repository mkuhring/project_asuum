02496_ORF1     	0.289	0.572	  0.001	   -
02496_ORF2     	0.117	0.251	  0.001	   -
06873_ORF1     	0.790	3.427	  0.007	   -
06873_ORF2     	0.705	2.210	  0.004	   -
38038_ORF1     	0.050	0.122	  0.000	   signal peptide predicted by SignalP
38038_ORF2     	0.443	0.955	  0.002	   signal peptide predicted by SignalP
38039_ORF1     	0.557	1.246	  0.002	   signal peptide predicted by SignalP
38040_ORF1     	0.557	1.246	  0.002	   signal peptide predicted by SignalP
38041_ORF1     	0.050	0.122	  0.000	   signal peptide predicted by SignalP
38041_ORF2     	0.443	0.955	  0.002	   signal peptide predicted by SignalP
38042_ORF1     	0.357	0.706	  0.001	   signal peptide predicted by SignalP
38043_ORF1     	0.228	0.644	  0.001	   signal peptide predicted by SignalP
38044_ORF1     	0.895	2.239	  0.004	   -
38044_ORF2     	0.362	1.143	  0.002	   -
38045_ORF1     	0.079	0.172	  0.000	   -
47796_ORF1     	0.784	3.056	  0.006	   -
47796_ORF2     	0.916	6.313	  0.013	   -
63629_ORF1     	0.386	0.745	  0.001	   -
75183_ORF1     	0.718	2.279	  0.005	   -
75184_ORF1     	0.900	5.688	  0.011	   -
75184_ORF2     	0.791	3.337	  0.007	   -
75184_ORF3     	0.811	4.016	  0.008	   signal peptide predicted by SignalP
75184_ORF4     	0.437	0.939	  0.002	   -
75185_ORF1     	0.811	4.016	  0.008	   signal peptide predicted by SignalP
75185_ORF2     	0.900	5.688	  0.011	   -
75185_ORF3     	0.791	0.817	  0.002	   -
75185_ORF4     	0.437	0.939	  0.002	   -
75186_ORF1     	0.862	4.622	  0.009	   -
75186_ORF2     	0.791	3.337	  0.007	   -
75186_ORF3     	0.811	4.016	  0.008	   signal peptide predicted by SignalP
75186_ORF4     	0.437	0.939	  0.002	   -
75187_ORF1     	0.791	3.337	  0.007	   -
75187_ORF2     	0.811	4.016	  0.008	   signal peptide predicted by SignalP
75187_ORF3     	0.437	0.939	  0.002	   -
83218_ORF1     	0.153	0.315	  0.001	   -
89892_ORF1     	0.641	1.759	  0.004	   -
90893_ORF1     	0.851	4.437	  0.009	   signal peptide predicted by SignalP
93871_ORF1     	0.366	0.866	  0.002	   -
GS_00202       	0.361	1.889	  0.004	   -
GS_00573       	0.596	5.566	  0.011	   signal peptide predicted by SignalP
GS_00653       	0.164	0.330	  0.001	   signal peptide predicted by SignalP
GS_01279       	0.629	2.290	  0.005	   -
GS_01534       	0.521	1.062	  0.002	   -
GS_02365       	0.716	1.995	  0.004	   signal peptide predicted by SignalP
GS_02546       	0.719	2.344	  0.005	   signal peptide predicted by SignalP
GS_02917       	0.739	2.734	  0.005	   -
GS_03085       	0.199	0.391	  0.001	   -
GS_04442       	0.701	2.204	  0.004	   -
GS_04485       	0.857	4.680	  0.009	   -
GS_05131       	0.667	1.758	  0.004	   -
GS_05408       	0.776	3.269	  0.007	   -
GS_05889       	0.850	4.574	  0.009	   signal peptide predicted by SignalP
GS_06005       	0.485	1.010	  0.002	   -
GS_06168       	0.679	1.983	  0.004	   signal peptide predicted by SignalP
GS_06576       	0.422	3.337	  0.007	   -
GS_08342       	0.858	4.551	  0.009	   signal peptide predicted by SignalP
GS_09091       	0.708	2.635	  0.005	   -
GS_09313       	0.594	1.355	  0.003	   -
GS_09942       	0.756	3.259	  0.007	   -
GS_10229       	0.886	5.279	  0.011	   -
GS_11879       	0.793	3.529	  0.007	   -
GS_12196       	0.870	5.150	  0.010	   -
GS_12432       	0.508	1.034	  0.002	   -
GS_13066       	0.144	0.298	  0.001	   -
GS_13324       	0.735	2.517	  0.005	   -
GS_13390       	0.483	0.959	  0.002	   -
GS_13763       	0.329	0.624	  0.001	   -
GS_15926       	0.527	1.105	  0.002	   -
GS_17433       	0.655	0.879	  0.002	   -
GS_17811       	0.539	1.281	  0.003	   -
GS_18003       	0.628	1.664	  0.003	   -
GS_18153       	0.830	3.870	  0.008	   -
GS_19483       	0.470	0.938	  0.002	   -
GS_21074       	0.711	2.132	  0.004	   -
GS_21329       	0.775	2.962	  0.006	   -
GS_21406       	0.501	1.010	  0.002	   -
GS_21488       	0.493	1.087	  0.002	   -
GS_22517       	0.866	4.764	  0.010	   signal peptide predicted by SignalP
GS_22558       	0.643	2.311	  0.005	   -
GS_22603       	0.192	0.380	  0.001	   -
GS_24029       	0.525	1.577	  0.003	   -
GS_24074       	0.431	0.829	  0.002	   -
L3E_01572      	0.779	3.019	  0.006	   -
L3E_03535      	0.346	0.643	  0.001	   -
L4_01652       	0.710	2.336	  0.005	   -
Liv_01241      	0.524	1.188	  0.002	   -
Liv_01297      	0.681	2.095	  0.004	   -
Liv_02052      	0.876	5.066	  0.010	   signal peptide predicted by SignalP
Liv_03967      	0.175	0.344	  0.001	   -