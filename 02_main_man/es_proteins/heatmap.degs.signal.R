# create heat map of time series log2 fold-changes

degs.file <- "worm.degs.es.proteins.txt"
output.pdf <- "plot.heatmap.leftovers.pdf"
output.png <- "plot.heatmap.leftovers.png"

library(reshape2)
library(ggplot2)

degs.data <- read.table(file = degs.file, header = TRUE, sep = "\t", quote = "", 
                        stringsAsFactors = FALSE, na.strings = c("NA", ""))

degs.data$Gene.ID <- sub(pattern = "gene_", replacement = "", x = degs.data$Gene.ID)


# check if any DEGs are available
if (nrow(degs.data) > 0){
    fc.idx <- grep("log2FoldChange", colnames(degs.data))
    fc.data <- degs.data[, fc.idx]
    fc.names <- gsub(pattern = "log2FoldChange_", replacement = "", colnames(degs.data)[fc.idx])
    colnames(fc.data) <- fc.names
    fc.data$Gene <- degs.data$Gene
    fc.data <- fc.data[1:min(nrow(fc.data), 100), ]

    fc.melt <- melt(fc.data, id.vars = "Gene")
    fc.melt$Gene <- factor(fc.melt$Gene, levels = sort(unique(fc.melt$Gene), decreasing = TRUE))

    p <- ggplot(fc.melt, aes(variable, Gene)) +
        geom_tile(aes(fill = value), colour = "white") +
        scale_fill_gradient2(low = "steelblue", mid = "white", high = "tomato", midpoint = 0) +
        labs(fill = "log2\nfold-change", x = "",y = "") +
        theme_grey(base_size = 12) +
        scale_x_discrete(expand = c(0, 0)) +
        scale_y_discrete(expand = c(0, 0)) +
        theme(legend.position = "right", axis.ticks = element_blank(), axis.text.x=element_text(angle=45,hjust=1))

    ggsave(filename = output.pdf, plot = p, width = 7, height = 0.6*nrow(fc.data)+1,
       units = "cm", scale = 1.5, limitsize = FALSE)
    ggsave(filename = output.png, plot = p, width = 7, height = 0.6*nrow(fc.data)+1,
       units = "cm", scale = 1.5, limitsize = FALSE, dpi = 600)
} else {
    p <- ggplot() + geom_blank()
    ggsave(filename = output.pdf, plot = p)
    ggsave(filename = output.png, plot = p)
}
