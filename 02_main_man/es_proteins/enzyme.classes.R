library(KEGGREST)
library(ggplot2)

# http://www.sbcs.qmul.ac.uk/iubmb/enzyme/
ec.classes <- c(
  "ec:1" = "Oxidoreductases",
  "ec:2" = "Transferases",
  "ec:3" = "Hydrolases",
  "ec:4" = "Lyases",
  "ec:5" = "Isomerases",
  "ec:6" = "Ligases"
)

# get KO to EC mappings
ko2enzyme <- keggLink("enzyme", "ko") # KO to path mapping
enzymes <- keggList("enzyme")         # EC descriptions
kos <- keggList("ko")                # KO descriptions

# import KEGG annotated DEGs
deg.kegg.file <- "../../02_main_eval/kegg/worm/degs.annotated_kegg.tsv"
deg.kegg.data <- read.table(deg.kegg.file, header = TRUE, sep = "\t", stringsAsFactor = FALSE)

# map ECs
names(ko2enzyme) <- sub(pattern = "ko:", replacement = "", names(ko2enzyme))
deg.kegg.data$EC <- ko2enzyme[deg.kegg.data$KEGG.KO]
deg.kegg.data$EC.Desc <- enzymes[deg.kegg.data$EC]

# filter by ES proteins
es.file <- "es.proteins.txt"
es.ids <- readLines(es.file)
deg.kegg.data.es <- subset(deg.kegg.data, subset = Gene.ID %in% es.ids)

# map EC classes
deg.kegg.data.es$EC.Class <- ec.classes[substr(deg.kegg.data.es$EC, 1, 4)]

# export ES with EC annotation
file.ec <- "worm.degs.es.ec.txt"
write.table(deg.kegg.data.es, file = file.ec, col.names = TRUE, row.names = FALSE, sep = "\t", quote = FALSE)

# make sure gene 2 EC mappings are unique
deg.kegg.data.es <- deg.kegg.data.es[!duplicated(deg.kegg.data.es[ ,c("Gene.ID", "EC")]), ]

# pie chart of EC classes
counts <- data.frame(table(deg.kegg.data.es$EC.Class, useNA = "ifany"))
names(counts) <- c("EC Class", "Frequency")
piechart <- ggplot(data = counts, mapping = aes(x = "", y = Frequency, fill = `EC Class`)) +
  geom_bar(stat = "identity", width = 1, alpha = I(0.75)) +
  coord_polar("y", start=0) +
  theme_void() + 
  geom_text(aes(y = sum(Frequency) - (Frequency/3 + c(0, cumsum(Frequency)[-length(Frequency)])), 
                label = Frequency), size = 5)
ggsave(filename = "plot.pie.es.ec.pdf", piechart, width = 5, height = 3, units = "in")

