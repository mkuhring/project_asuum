# A. suum Dual RNA-Seq Time Series Analysis

This is a configuration repository of the data analysis conducted for the
publication

`Silent witness: Dual-species transcriptomics reveals epithelial immunological quiescence to Ascaris suum larvae and fostered larval development`  
by Friederike Ebner, Mathias Kuhring, Aleksandar Radonić, Ankur Midha,
Bernhard Y. Renard, Susanne Hartmann (2018, in submission)

The raw read data of this experiment is deposited in the 
[Sequence Read Archive (SRA)](https://www.ncbi.nlm.nih.gov/sra) and 
collectively available via the BioProject
[PRJNA450204](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA450204).

## Analyses Workflow

The main part of the analysis relies on automated pipelines, including read 
quality control, differential expression, annotation and functional analysis.

1. Raw reads are trimmed and evaluated using the
[QCumber pipeline](https://gitlab.com/RKIBioinformaticsPipelines/QCumber)
with the script [run_qc.sh](experiment_main/run_qc.sh).

2. Read mapping, counting and differential expression analysis is performed with 
our 
[Differential Expression Pipeline](https://gitlab.com/mkuhring/pipeline_de/tree/9f4f5c935c53f41d1b64bdb1e67b9f83d4d42815) 
according to the configuration in 
[02_main_de](02_main_de/snakemake.config.json).

3. Annotation of novel genes as well as re-annotation of references genes is 
performed with our
[Sequence Annotation Pipeline](https://gitlab.com/mkuhring/pipeline_sa/tree/6b0b9ded663a037178b69f8f4abf318fc839a79a)
according to the configuration in 
[02_main_anno](02_main_anno/snakemake.config.json).

4. Functional analysis is performed using our 
[Function Evaluation Pipeline](https://gitlab.com/mkuhring/pipeline_fe/tree/37d48dc9f1cb05e3ec6892af0fab336a607935f5)
according to the configuration in 
[02_main_eval](02_main_eval/snakemake.config.json).

Additional (manual) analysis, data and corresponding code is located in 
[02_main_man](02_main_man). This includes:

* Dual species co-expression analysis in
[02_main_man/co-expression](02_main_man/co-expression)

* Identification of excretory-secretory proteins in
[02_main_man/es_proteins](02_main_man/es_proteins)

* Comparison to reference immunomodulatory proteins in
[02_main_man/immune_regulation](02_main_man/immune_regulation)